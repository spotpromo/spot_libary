package com.spotpromo.spot_libary.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Biblioteca : Serializable {
    @SerializedName("codBiblioteca")
    var codBiblioteca: Int? = null
    @SerializedName("desBiblioteca")
    var desBiblioteca: String? = null
    @SerializedName("codBibliotecaCategoria")
    var codBibliotecaCategoria: Int? = null
    @SerializedName("desBibliotecaCategoria")
    var desBibliotecaCategoria: String? = null
    @SerializedName("descricao")
    var descricao: String? = null
    @SerializedName("nomeArquivo")
    var nomeArquivo: String? = null
    @SerializedName("codSubBu")
    var codSubBu: Int? = null
    @SerializedName("codPessoa")
    var codPessoa: Int? = null
    @SerializedName("flLido")
    var flLido: Int? = null


    var download_arquivo: DownloadArquivo? = null
}