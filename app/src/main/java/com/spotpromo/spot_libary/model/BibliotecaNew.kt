package com.spotpromo.spot_libary.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
class BibliotecaNew(
    @Json(name = "codBiblioteca") var codBiblioteca: Int? = null,
    @Json(name = "desBiblioteca") var desBiblioteca: String? = null,
    @Json(name = "codBibliotecaCategoria") var codBibliotecaCategoria: Int? = null,
    @Json(name = "desBibliotecaCategoria") var desBibliotecaCategoria: String? = null,
    @Json(name = "descricao") var descricao: String? = null,
    @Json(name = "nomeArquivo") var nomeArquivo: String? = null,
    @Json(name = "codSubBu") var codSubBu: Int? = null,
    @Json(name = "codPessoa") var codPessoa: Int? = null,
    @Json(name = "flLido") var flLido: Int? = null
) : Parcelable