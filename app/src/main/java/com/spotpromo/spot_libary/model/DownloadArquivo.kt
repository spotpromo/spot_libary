package com.spotpromo.spot_libary.model

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

class DownloadArquivo() : Parcelable {

    var progress: Int = 0
    var currentFileSize: Int = 0
    var totalFileSize: Int = 0
    var onSuccess: Boolean = false
    var msgError: String? = null
    var posicao: Int? = null
    var onStarted = false
    var uriFile: Uri? = null
    var mimeType: String? = ""

    constructor(parcel: Parcel) : this() {
        progress = parcel.readInt()
        currentFileSize = parcel.readInt()
        totalFileSize = parcel.readInt()
        onSuccess = parcel.readByte() != 0.toByte()
        msgError = parcel.readString()
        posicao = parcel.readValue(Int::class.java.classLoader) as? Int
        onStarted = parcel.readByte() != 0.toByte()
        uriFile = parcel.readParcelable(Uri::class.java.classLoader) as? Uri
        mimeType = parcel.readString()
    }


    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest!!.writeInt(progress)
        dest!!.writeInt(currentFileSize)
        dest!!.writeInt(totalFileSize)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DownloadArquivo> {
        override fun createFromParcel(parcel: Parcel): DownloadArquivo {
            return DownloadArquivo(parcel)
        }

        override fun newArray(size: Int): Array<DownloadArquivo?> {
            return arrayOfNulls(size)
        }
    }
}