package com.spotpromo.spot_libary.downloadservice

import android.app.IntentService
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.spotpromo.spot_libary.R
import com.spotpromo.spot_libary.config.SpotLibaryConfig
import com.spotpromo.spot_libary.model.DownloadArquivo
import com.spotpromo.spot_libary.view.ActivityBiblioteca
import com.spotpromo.spot_libary.webclient.Conexao
import okhttp3.ResponseBody
import java.io.*
import java.lang.Exception

class DownloadArquivoBiblioteca : IntentService("download_file") {


    override fun onHandleIntent(intent: Intent?) {

        val arquivo = intent!!.getStringExtra("nomearquivo")
        val posicao = intent!!.getIntExtra("posicao", 0)
        init_download(arquivo!!, posicao)

    }


    private fun init_download(arquivo: String, posicao: Int) {
        try {
            /**
             * INSTANCIA OBJETO RETROFIT
             */

            val request = Conexao().download(
                SpotLibaryConfig.url_download_arquivo,
                arquivo
            )

            escrever_arquivo(request.execute().body()!!, arquivo, posicao)
        } catch (err : Exception) {
            onDownloadFailed(
                RuntimeException(
                    this.resources.getString(R.string.biblioteca_download_arquivo_biblioteca_erro).replace(
                        "_ARQUIVO",
                        arquivo
                    )
                ), posicao
            )

        }


    }


    private fun escrever_arquivo(body: ResponseBody, arquivo: String, posicao: Int): Boolean {

        try {

            if(body.contentLength() <= 0) {
                throw RuntimeException(
                    this.resources.getString(R.string.biblioteca_download_arquivo_biblioteca_erro).replace(
                        "_ARQUIVO",
                        arquivo
                    )
                )
            }

//            val file_arquivo = File(
//                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
//                "arquivo_download" + File.separator + arquivo
//            )
            val file_arquivo = File(
                getExternalFilesDir(null),
                //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
                arquivo
            )

            var _in: InputStream? = null
            var _out: OutputStream? = null

            val startTime = System.currentTimeMillis()
            var timeCount = 1

            try {
                var fileReader = ByteArray(4096)
                var read: Int
                var fileSize = body.contentLength()
                var fileSizeDownload : Long = 0

                /** INPUT STREAM PARA LER O TAMANHO DO ARQUIVO **/
                _in = body.byteStream()
                /** CRIAR ARQUIVO NO DIRETORIO ESPECIFICADO **/
                _out = FileOutputStream(file_arquivo)

                /**
                 * LOOP
                 */

                do {
                    read = _in!!.read(fileReader)

                    if(read == -1)
                        break

                    fileSizeDownload += read

                    /** TOTAL ARQUIVO **/
                    val totalFileSize = (fileSize / (Math.pow(1024.0, 2.0))).toInt()
                    /** PEGA TOTAL ESCRITO **/
                    val current = Math.round(fileSizeDownload / (Math.pow(1024.0, 2.0))).toDouble()
                    /** PEGA TAMANHO DO PROGRESS **/
                    val progress = ((fileSizeDownload * 100) / fileSize).toInt()
                    /** VARIAVEL UTILIZADA PARA PAUSAR O BROADCAST **/
                    val currentTime = System.currentTimeMillis() - startTime

                    val download = DownloadArquivo()
                    download.totalFileSize = totalFileSize
                    download.onSuccess = true
                    download.posicao = posicao
                    if (currentTime > 500 * timeCount) {
                        download.currentFileSize = current.toInt()
                        download.progress = progress
                        sendNotification(download)
                        timeCount++
                    }

                    _out.write(fileReader, 0, read)

                } while (true)


                //mensagem e saida para instalação da apk
                onDownloadComplete(posicao)
                _out.flush()

                return true

            } catch (e: IOException) {
                onDownloadFailed(
                    RuntimeException(
                        this.resources.getString(R.string.biblioteca_download_arquivo_biblioteca_erro).replace(
                            "_ARQUIVO",
                            arquivo
                        )
                    ), posicao
                )
                return false
            } finally {
                _in?.close()
                _out?.close()
            }
        } catch (e: IOException) {
            onDownloadFailed(
                RuntimeException(
                    this.resources.getString(R.string.biblioteca_download_arquivo_biblioteca_erro).replace(
                        "_ARQUIVO",
                        arquivo
                    )
                ), posicao
            )
            return false
        }

    }


    /**
     * ENVIAR NOTIFICAÇÃO PARA TELA DE ATUALIZAÇÃO
     *
     * @param download
     */
    private fun sendNotification(download: DownloadArquivo) {
        sendIntent(download)
    }

    /**
     * ENVIA UM INTENT COM O OBJETO DOWNLOAD
     *
     * @param download
     */
    private fun sendIntent(download: DownloadArquivo) {
        val intent = Intent(ActivityBiblioteca.MESSAGE_PROGRESS)
        intent.putExtra("downloadarquivo", download)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    /**
     * METODO PARA DISPARAR MENSAGEM QUANDO DOWNLOAD FOR CONCLUIDO
     */
    private fun onDownloadComplete(posicao: Int?) {

        val download = DownloadArquivo()
        download.progress = 100
        download.onSuccess = true
        download.posicao = posicao
        sendIntent(download)

    }

    /**
     * METODO CASO OCORRA ALGUM ERRO NO DOWNLOAD DA NOVA VERSAO
     *
     * @param e
     */
    private fun onDownloadFailed(e: Exception, posicao: Int?) {
        val download = DownloadArquivo()
        download.onSuccess = false
        download.progress = 0
        download.msgError = e.message
        download.posicao = posicao
        sendIntent(download)

    }
}