package com.spotpromo.spot_libary.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.pnikosis.materialishprogress.ProgressWheel
import com.spotpromo.spot_libary.R
import com.spotpromo.spot_libary.adapter.AdapterBibliotecaCategoria
import com.spotpromo.spot_libary.adapter.RecycleBiblioteca
import com.spotpromo.spot_libary.dao.BibliotecaDAOHelper
import com.spotpromo.spot_libary.model.Biblioteca
import com.spotpromo.spot_libary.model.DownloadArquivo
import com.spotpromo.spot_util.utils.LogTrace
import com.spotpromo.spot_util.utils.SendIntent
import com.spotpromo.spot_util.utils.Util
import com.spotpromo.spot_util.utils.databasehelper.SqliteDataBaseHelper
import kotlinx.android.synthetic.main.layout_biblioteca.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

class ActivityBiblioteca : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    var lista = ArrayList<Biblioteca>()
    var mRecycle = RecycleBiblioteca(this@ActivityBiblioteca, lista)
    lateinit var classFrom : Class<Any>

    companion object {
        var MESSAGE_PROGRESS = "message_progress_download"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_biblioteca)
        try {
            controles()
        } catch (err: Exception) {
            LogTrace.logCatch(this@ActivityBiblioteca, this.javaClass, err, true)
        }
    }

    @Throws(Exception::class)
    private fun controles() {
        toolbar.title = resources.getString(R.string.app_name)
        toolbar.subtitle = resources.getString(R.string.biblioteca)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val recycle = findViewById(R.id.recycle) as RecyclerView
        recycle.setHasFixedSize(true)
        (recycle.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        val mLayoutManager =
            LinearLayoutManager(this@ActivityBiblioteca)
        recycle.layoutManager = mLayoutManager
        recycle.addItemDecoration(
            DividerItemDecoration(
                this@ActivityBiblioteca,
                DividerItemDecoration.VERTICAL
            )
        )
        recycle.adapter = mRecycle

        val db = SqliteDataBaseHelper.openDB(this@ActivityBiblioteca)
        val daBiblioteca = BibliotecaDAOHelper(db)
        val lista_adapter = daBiblioteca.select_categoria()

        spn_categoria.onItemSelectedListener = this
        spn_categoria.adapter = AdapterBibliotecaCategoria(this@ActivityBiblioteca, lista_adapter)

        registrar()
        hide()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        try {
            when(parent) {
                spn_categoria -> {
                    val biblioteca_categoria = spn_categoria.getItemAtPosition(position) as Biblioteca
                    if(biblioteca_categoria.codBibliotecaCategoria != 0) {
                        chama_task(biblioteca_categoria.codBibliotecaCategoria!!)
                    } else {
                        lista.clear()
                        onRetorno(false, "")
                    }
                }
            }
        } catch (err : Exception) {
            LogTrace.logCatch(this@ActivityBiblioteca, this.javaClass, err, true)
        }
    }


    fun show() {
        var txtnotfoud: TextView = findViewById(R.id.txtnotfound)
        var mprogress: ProgressWheel = findViewById(R.id.mprogress)
        txtnotfoud.visibility = View.GONE
        mprogress.visibility = View.VISIBLE
    }

    fun hide() {
        var mprogress: ProgressWheel = findViewById(R.id.mprogress)
        mprogress.visibility = View.GONE
    }


    private fun chama_task(codBibliotecaCategoria: Int) {
        doAsync {

            uiThread {

                show()
                lista.clear()
                mRecycle.notifyDataSetChanged()

                var status = false
                var mensagem = ""

                try {
                    var db = SqliteDataBaseHelper.openDB(this@ActivityBiblioteca)
                    lista.addAll(BibliotecaDAOHelper(db).select(codBibliotecaCategoria, this@ActivityBiblioteca))
                    SqliteDataBaseHelper.closeDB(db)

                    if (lista.isNotEmpty())
                        status = true
                } catch (err: Exception) {
                    LogTrace.logCatch(this@ActivityBiblioteca, this.javaClass, err, false)
                } finally {
                    hide()
                    onRetorno(status, mensagem)
                }
            }
        }
    }


    fun onRetorno(aBoolean: Boolean, mensagem: String) {
        var txtnotfoud: TextView = findViewById(R.id.txtnotfound)
        txtnotfoud.visibility = View.VISIBLE
        if (aBoolean)
            txtnotfoud.visibility = View.GONE

        mRecycle.notifyDataSetChanged()
    }

    /**
     * METHODO PARA ESCUTAR O BROAD CAST
     */
    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                if (intent.action == MESSAGE_PROGRESS) {

                    val download = intent.getParcelableExtra<DownloadArquivo>("downloadarquivo")

                    if (!download!!.onSuccess)
                        Util.mensagemSnack(
                            findViewById<CoordinatorLayout>(R.id.mcoord),
                            download.msgError!!,
                            R.color.vermelho,
                            0
                        )

                    /** SETA O VALOR DO RETORNO NO OBJETO DE ACORDO COM A POSICAO  */
                    lista[download.posicao!!].download_arquivo = download

                    mRecycle.notifyItemChanged(download.posicao!!)
                }
            } catch (err: Exception) {
                Log.e("ERRO_DOWNLOAD", err.message!!)
            } finally {

            }

        }
    }

    private fun registrar() {
        val manager = LocalBroadcastManager.getInstance(this@ActivityBiblioteca)
        val intent = IntentFilter()
        intent.addAction(MESSAGE_PROGRESS)
        manager.registerReceiver(broadcastReceiver, intent)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                SendIntent.with()
                    .mClassFrom(this@ActivityBiblioteca)
                    .mClassTo(classFrom)
                    .mType(R.integer.slide_from_left)
                    .go()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        SendIntent.with()
            .mClassFrom(this@ActivityBiblioteca)
            .mClassTo(classFrom)
            .mType(R.integer.slide_from_left)
            .go()
    }
}