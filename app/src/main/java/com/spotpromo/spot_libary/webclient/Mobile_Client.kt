package com.spotpromo.spot_libary.webclient

import com.spotpromo.spot_libary.model.DadosJson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.util.HashMap

interface Mobile_Client {
    @FormUrlEncoded
    @POST
    fun downloadFileUrl(@Url url: String, @Field("nomeArquivo") nomeArquivo: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST
    fun post(@Url url: String, @FieldMap data: HashMap<String, String>): Call<DadosJson>
}