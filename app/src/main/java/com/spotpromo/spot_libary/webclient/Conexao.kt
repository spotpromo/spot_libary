package com.spotpromo.spot_libary.webclient

import com.spotpromo.spot_libary.config.SpotLibaryConfig
import com.spotpromo.spot_libary.model.DadosJson
import com.spotpromo.spot_util.utils.webclient.retrofit.Retrofit_Client
import okhttp3.ResponseBody
import retrofit2.Call

class Conexao : Retrofit_Client() {
    fun download(url: String, nome_arquivo: String): Call<ResponseBody> {
        val dados = getClient(SpotLibaryConfig.url_principal, Mobile_Client::class.java) as Mobile_Client
        return dados.downloadFileUrl(url, nome_arquivo)
    }

    fun get(url: String, data: HashMap<String, String>): Call<DadosJson> {
        val dados = getClient(SpotLibaryConfig.url_principal, Mobile_Client::class.java) as Mobile_Client
        return dados.post(url, data)
    }
}