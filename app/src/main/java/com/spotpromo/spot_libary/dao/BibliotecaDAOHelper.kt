package com.spotpromo.spot_libary.dao

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.spotpromo.spot_libary.R
import com.spotpromo.spot_libary.model.Biblioteca
import com.spotpromo.spot_libary.model.BibliotecaNew
import com.spotpromo.spot_libary.model.DownloadArquivo
import com.spotpromo.spot_util.utils.Util
import com.spotpromo.spot_util.utils.databasehelper.DAOHelper
import java.io.File
import java.util.ArrayList

class BibliotecaDAOHelper @Throws(Exception::class)
constructor(db: SQLiteDatabase) : DAOHelper(db) {
    /**
     * METODO INSERIR
     *
     * @param configuracao
     * @throws Exception
     */
    @Throws(Exception::class)
    fun insert(biblioteca: Biblioteca) {
        val sbQuery = StringBuilder()
        sbQuery.append("INSERT INTO TB_Biblioteca (codBiblioteca, desBiblioteca, codBibliotecaCategoria, desBibliotecaCategoria, descricao, nomeArquivo, codSubBu, codPessoa, flLido) VALUES(?,?,?,?,?,?,?,?,?)")

        this.executaNoQuery(
            sbQuery.toString(),
            biblioteca.codBiblioteca!!,
            biblioteca.desBiblioteca!!,
            biblioteca.codBibliotecaCategoria!!,
            biblioteca.desBibliotecaCategoria!!,
            biblioteca.descricao!!,
            biblioteca.nomeArquivo!!,
            biblioteca.codSubBu!!,
            biblioteca.codPessoa!!,
            biblioteca.flLido!!
        )
    }

    @Throws(Exception::class)
    fun insert(biblioteca: BibliotecaNew) {
        val sbQuery = StringBuilder()
        sbQuery.append("INSERT INTO TB_Biblioteca (codBiblioteca, desBiblioteca, codBibliotecaCategoria, desBibliotecaCategoria, descricao, nomeArquivo, codSubBu, codPessoa, flLido) VALUES(?,?,?,?,?,?,?,?,?)")

        this.executaNoQuery(
            sbQuery.toString(),
            biblioteca.codBiblioteca!!,
            biblioteca.desBiblioteca!!,
            biblioteca.codBibliotecaCategoria!!,
            biblioteca.desBibliotecaCategoria!!,
            biblioteca.descricao!!,
            biblioteca.nomeArquivo!!,
            biblioteca.codSubBu!!,
            biblioteca.codPessoa!!,
            biblioteca.flLido!!
        )
    }

    /**
     * DELETAR TUDO
     *
     * @return
     */
    @Throws(java.lang.Exception::class)
    fun deletar() {
        val tabelas = arrayOf(
            "TB_Biblioteca"
        )

        for (i in tabelas.indices) {
            //Monta Query
            val query = String.format("DELETE FROM %s", tabelas[i])
            this.executaNoQuery(query)
        }
    }

    @Throws(Exception::class)
    fun update(codBiblioteca : Int?) {
        val sbQuery = StringBuilder()
        sbQuery.append("UPDATE TB_Biblioteca SET flLido = 1 WHERE codBiblioteca IN (?) ")
        this.executaNoQuery(sbQuery.toString(), codBiblioteca!!)
    }

    /**
     * INSERT LISTA
     * @param lista
     * @param context
     * @throws Exception
     */
    @Throws(Exception::class)
    fun inserir(lista: ArrayList<Biblioteca>, context: Context) {
        Util.manipulaLoading(context, lista.size, "", 3)
        for (i in lista.indices) {
            insert(lista[i])
            Util.manipulaLoading(
                context,
                0,
                String.format(
                    context.resources.getString(R.string.loading_manipulacao).replace(
                        "_TIPO",
                        context.resources.getString(R.string.msg_insert_dados_biblioteca)
                    ), i, lista.size
                ),
                1
            )
            Util.manipulaLoading(context, i, "", 2)
        }
    }

    @Throws(Exception::class)
    fun inserirNew(lista: ArrayList<BibliotecaNew>) {
        for (i in lista.indices) {
            insert(lista[i])
        }
    }

    /**
     * SELECT
     * @return
     */
    @Throws(Exception::class)
    fun select(codCategoria : Int, context: Context): ArrayList<Biblioteca> {
        var cursor: Cursor? = null

        val sbQuery = StringBuilder()

        sbQuery.append("SELECT * FROM TB_Biblioteca WHERE codBibliotecaCategoria IN (?) ORDER BY desBiblioteca ASC ")

        cursor = this.executaQuery(sbQuery.toString(), codCategoria)
        var lista = ArrayList<Biblioteca>()
        if (cursor.moveToFirst()) {
            do {
                var  biblioteca = Biblioteca()
                biblioteca.codBiblioteca = cursor.getInt(cursor.getColumnIndex("codBiblioteca"))
                biblioteca.desBiblioteca = cursor.getString(cursor.getColumnIndex("desBiblioteca"))
                biblioteca.codBibliotecaCategoria = cursor.getInt(cursor.getColumnIndex("codBibliotecaCategoria"))
                biblioteca.desBibliotecaCategoria = cursor.getString(cursor.getColumnIndex("desBibliotecaCategoria"))
                biblioteca.descricao = cursor.getString(cursor.getColumnIndex("descricao"))
                biblioteca.nomeArquivo = cursor.getString(cursor.getColumnIndex("nomeArquivo"))
                biblioteca.codSubBu = cursor.getInt(cursor.getColumnIndex("codSubBu"))
                biblioteca.codPessoa = cursor.getInt(cursor.getColumnIndex("codPessoa"))
                biblioteca.flLido = cursor.getInt(cursor.getColumnIndex("flLido"))

                //val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), File.separator + "arquivo_download" + File.separator + biblioteca.nomeArquivo)
                val file = File(context.getExternalFilesDir(null), biblioteca.nomeArquivo)

                if(file.isFile) {
                    val download = DownloadArquivo()
                    download.progress = 100
                    download.onSuccess = true
                    biblioteca.download_arquivo = download
                }

                lista.add(biblioteca)
            } while (cursor.moveToNext())
        }

        cursor.close()

        return lista
    }

    /**
     * SELECT
     * @return
     */
    @Throws(Exception::class)
    fun select_categoria(): ArrayList<Biblioteca> {
        var cursor: Cursor? = null

        val sbQuery = StringBuilder()

        sbQuery.append("SELECT DISTINCT codBibliotecaCategoria, desBibliotecaCategoria FROM TB_Biblioteca ORDER BY desBibliotecaCategoria ASC ")

        cursor = this.executaQuery(sbQuery.toString())
        var lista = ArrayList<Biblioteca>()
        lista.add(selecione())
        if (cursor.moveToFirst()) {
            do {
                var  biblioteca = Biblioteca()
                biblioteca.codBibliotecaCategoria = cursor.getInt(cursor.getColumnIndex("codBibliotecaCategoria"))
                biblioteca.desBibliotecaCategoria = cursor.getString(cursor.getColumnIndex("desBibliotecaCategoria"))
                lista.add(biblioteca)
            } while (cursor.moveToNext())
        }

        cursor?.close()

        return lista
    }

    /**
     * SELECT
     * @return
     */
    @Throws(Exception::class)
    fun select(): Int {

        var qtd: Int = 0
        var cursor: Cursor?

        val sbQuery = StringBuilder()

        sbQuery.append("SELECT * FROM TB_Biblioteca ")

        cursor = this.executaQuery(sbQuery.toString())
        qtd = cursor.count

        cursor.close()

        return qtd
    }

    private fun selecione() : Biblioteca {
        var biblioteca = Biblioteca()
        biblioteca.codBibliotecaCategoria = 0
        biblioteca.desBibliotecaCategoria = "SELECIONE"
        return biblioteca
    }
}