package com.spotpromo.spot_libary.adapter

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.spotpromo.spot_libary.R
import com.spotpromo.spot_libary.config.SpotLibaryConfig
import com.spotpromo.spot_libary.downloadservice.DownloadArquivoBiblioteca
import com.spotpromo.spot_libary.model.Biblioteca
import com.spotpromo.spot_libary.task.TaskEnviaBibliotecaLido
import com.spotpromo.spot_util.utils.Alerta
import com.spotpromo.spot_util.utils.LogTrace
import com.spotpromo.spot_util.utils.SupportImagem
import com.spotpromo.spot_util.utils.Util
import java.io.File
import java.lang.StringBuilder
import java.util.ArrayList
import java.lang.Exception


class RecycleBiblioteca(private val mContext: Context, private val mLista: ArrayList<Biblioteca>) :
    RecyclerView.Adapter<RecycleBiblioteca.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        internal var lnStatus: LinearLayout
        internal var imgDownload: LinearLayout
        internal var imgDownloadProgress: LinearLayout
        internal var mRow: LinearLayout
        internal var nome: TextView
        internal var mProgress: ProgressBar
        internal var imgTipo: ImageView

        init {
            nome = v.findViewById<View>(R.id.nome) as TextView
            mRow = v.findViewById<View>(R.id.mrow) as LinearLayout
            lnStatus = v.findViewById<View>(R.id.ln_status) as LinearLayout
            imgDownload = v.findViewById<View>(R.id.download) as LinearLayout
            imgDownloadProgress = v.findViewById<View>(R.id.download_progress) as LinearLayout
            lnStatus = v.findViewById<View>(R.id.ln_status) as LinearLayout
            mProgress = v.findViewById<View>(R.id.progressbarcircular) as ProgressBar
            imgTipo = v.findViewById<View>(R.id.img_tipo) as ImageView
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycle_biblioteca, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        try {
            val biblioteca = mLista[i]

            var sbHtml = StringBuilder()
            sbHtml.append(String.format("<strong>%s</strong>", biblioteca.desBiblioteca))
            sbHtml.append(
                String.format(
                    "<br/><br/><b>%s:</b> %s",
                    mContext.resources.getString(R.string.biblioteca_descricao),
                    biblioteca.descricao
                )
            )

            holder.nome.text = Html.fromHtml(sbHtml.toString()).toString()

            var status = validaArquivo(biblioteca.nomeArquivo!!, biblioteca)

            holder.lnStatus.setBackgroundColor(Color.RED)
            holder.lnStatus.invalidate()
            inativeView(View.GONE, holder.imgDownload, holder.imgDownloadProgress)

            /** VALIDA TIPO ARQUIVO **/

            if (biblioteca.nomeArquivo!!.indexOf(".pdf") > 0)
                holder.imgTipo.background = mContext.resources.getDrawable(R.drawable.ic_pdf_preto)
            else if (biblioteca.nomeArquivo!!.indexOf(".png") > 0 || biblioteca.nomeArquivo!!.indexOf(".jpg") > 0 ||  biblioteca.nomeArquivo!!.indexOf(".jpeg") > 0)
                holder.imgTipo.background = mContext.resources.getDrawable(R.drawable.ic_imagem)
            else if (biblioteca.nomeArquivo!!.indexOf(".mp4") > 0 || biblioteca.nomeArquivo!!.indexOf(".mp3") > 0)
                holder.imgTipo.background = mContext.resources.getDrawable(R.drawable.ic_video)

            when (status) {
                false -> {
                    if (biblioteca.download_arquivo != null && biblioteca.download_arquivo!!.onSuccess) {
                        inativeView(View.GONE, holder.imgDownload)
                        inativeView(View.VISIBLE, holder.imgDownloadProgress)
                        holder.mProgress.progress = biblioteca.download_arquivo!!.progress

                    } else {
                        inativeView(View.VISIBLE, holder.imgDownload)
                        inativeView(View.GONE, holder.imgDownloadProgress)
                        holder.mProgress.progress = 0
                    }

                    holder.imgDownload.setOnClickListener(mClickDownload(i))
                }
                else -> {
                    holder.lnStatus.setBackgroundColor(Color.GREEN)
                    holder.lnStatus.invalidate()

                    inativeView(View.GONE, holder.imgDownload, holder.imgDownloadProgress)

                    holder.mRow.setOnClickListener(mClick(i))
                }

            }

        } catch (err: Exception) {
            LogTrace.logCatch(mContext, mContext.javaClass, err, true)
        }
    }

    /**
     * METODO ONCLICK
     *
     * @param position
     * @return
     */
    private fun mClickDownload(position: Int): View.OnClickListener {
        return View.OnClickListener { v ->
            try {
                val biblioteca = mLista[position]

                if (biblioteca.download_arquivo == null || !biblioteca.download_arquivo!!.onSuccess)
                    Alerta.show(
                        v.context,
                        v.context.resources.getString(R.string.biblioteca_download_arquivo_pdf_alerta),
                        mContext.resources.getString(R.string.biblioteca_download_arquivo_pdf_alerta_mensagem).replace(
                            "_ARQUIVO",
                            biblioteca.desBiblioteca!!
                        ),
                        mContext.resources.getString(R.string.btn_sim),
                        DialogInterface.OnClickListener { dialog, which ->
                            try {

                                (mContext as Activity).startService(
                                    Intent(mContext, DownloadArquivoBiblioteca::class.java)
                                        .putExtra("nomearquivo", biblioteca.nomeArquivo)
                                        .putExtra("posicao", position)
                                )
                            } catch (err: Exception) {
                                LogTrace.logCatch(mContext, mContext.javaClass, err, true)
                            }
                        },
                        false
                    )

            } catch (err: Exception) {
                LogTrace.logCatch(mContext, mContext.javaClass, err, true)
            }
        }
    }

    /**
     * METODO ONCLICK
     *
     * @param position
     * @return
     */
    private fun mClick(position: Int): View.OnClickListener {
        return View.OnClickListener { v ->
            try {
                val biblioteca = mLista[position]
//                val file = File(
//                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
//                    File.separator + "arquivo_download" + File.separator + biblioteca.nomeArquivo
//                )
                val file = File(
                    v.context.getExternalFilesDir(null),
                    biblioteca.nomeArquivo
                )
                var intent = Intent(Intent.ACTION_VIEW)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    intent.data = FileProvider.getUriForFile(mContext, SpotLibaryConfig.app_build_id + ".provider", file)
                    intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                } else {
                    val mimeType = Util.retornaMimeType(file, mContext)
                    intent.setDataAndType(Uri.fromFile(file), mimeType)
                    intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                    intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                }

                if (file.name.indexOf(".jpg") > 0 || file.name.indexOf(".png") > 0) {
                    v.isClickable = false
                    Util.showImageGallery(mContext, SupportImagem.ajustOrientation(file, 1024, 980)!!, v)
                } else
                    mContext.startActivity(intent)

                if (biblioteca.flLido == 0)
                    TaskEnviaBibliotecaLido(mContext).execute(biblioteca.codBiblioteca)

            } catch (err: Exception) {
                Alerta.show(
                    mContext,
                    mContext.resources.getString(R.string.msg_atencao),
                    mContext.resources.getString(R.string.biblioteca_download_arquivo_erro_abrir),
                    false
                )
                LogTrace.logCatch(mContext, mContext.javaClass, err, false)
            }
        }
    }

    override fun getItemCount(): Int {
        return mLista.size
    }

    @Throws(Exception::class)
    private fun inativeView(type: Int, vararg v: View) {
        for (view in v)
            view.visibility = type
    }

    /**
     * VERIFICA SE O ARQUIVO ESTA NA PASTA E SE O DOWNLOAD ESTA COMPLETO
     */
    @Throws(Exception::class)
    private fun validaArquivo(nomeArquivo: String, biblioteca: Biblioteca): Boolean {
//        val diretorio =
//            File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + File.separator + "arquivo_download")

//        if (!diretorio.isDirectory)
//            diretorio.mkdirs()

       // val arquivo = File(diretorio.absolutePath + File.separator + nomeArquivo)

        val arquivo = File(mContext.getExternalFilesDir(null), nomeArquivo)

        return arquivo.isFile && biblioteca.download_arquivo != null && biblioteca.download_arquivo!!.progress == 100

    }
}