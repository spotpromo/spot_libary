package com.spotpromo.spot_libary.task

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import com.spotpromo.spot_libary.config.SpotLibaryConfig
import com.spotpromo.spot_libary.dao.BibliotecaDAOHelper
import com.spotpromo.spot_libary.webclient.Conexao
import com.spotpromo.spot_util.utils.LogTrace
import com.spotpromo.spot_util.utils.Util
import com.spotpromo.spot_util.utils.databasehelper.SqliteDataBaseHelper

class TaskEnviaBibliotecaLido
constructor(context: Context) :
    AsyncTask<Int, Void, Boolean>() {

    var context: Context
    var mensagem: String = ""

    init {
        this.context = context
    }

    override fun onPreExecute() {
        super.onPreExecute()
    }

    @SuppressLint("WrongThread")
    override fun doInBackground(vararg params: Int?): Boolean {
        try {
            Thread.sleep(200)

            val request = Conexao().get(
                SpotLibaryConfig.url_envia_biblioteca_lido, Util.retornaHashJson("=",
                    String.format("codBiblioteca=%s", params[0]),
                    String.format("codPessoa=%s", SpotLibaryConfig.codPessoa),
                    String.format("codSubBu=%s", SpotLibaryConfig.codSubBu)
                )
            )

            val response = request.execute()

            /** VERIFICA SE A CONEXAO FOI SUCESSO **/
            if (!response.isSuccessful)
                return false

            /** VERIFICA SE O OBJETO MENSAGEM NAO ESTA NULO **/
            if (response.body()!!.mMensagem == null)
                return false

            val mensagemJson = response.body()!!.mMensagem!!

            if (mensagemJson.status != 1)
                return false

            val db = SqliteDataBaseHelper.openDB(context)
            BibliotecaDAOHelper(db).update(params[0])
            SqliteDataBaseHelper.closeDB(db)

            return true

        } catch (err: Exception) {
            mensagem = LogTrace.logCatch(context, context.javaClass, err, false)
            this.onCancelled()
        }

        return false
    }

    override fun onPostExecute(result: Boolean) {
        super.onPostExecute(result)
    }

}